from src import strats
from src import utils


def main_handler(config):

    strat, env = config.get('strat'), config.get('env')
    identifier = utils.generate_identifier()

    if strat == "mcext_rr":
        from src.strats import mcext_rr
        strats.mcext_rr.mcext_rr(env, identifier)


def lambda_handler(event, context):

    event.update({"env": "lambda"})
    main_handler(event)


if __name__ == '__main__':
    x = {"strat": "mcext_rr"}
    x.update({"env": "manual"})
    main_handler(x)


