from src import configs
from src.configs import mcext_rr as cfg
from src import utils
import pandas as pd
from datetime import datetime
import os
import pymssql
import math


def get_running_status():
    ms_conn = pymssql.connect(**configs.con_settings.get("ms"))
    status = pd.read_sql(cfg.running_status_query, con=ms_conn).status.values[0]
    ms_conn.close()

    return status


def update_running_status(status):
    curent_ts_utc = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    query = cfg.update_run_stat_query.format(curent_ts_utc, status)
    utils.execute_query(query)


def get_ix_range():
    with pymssql.connect(**configs.con_settings.get("ms")) as con:
        df_max_index = pd.read_sql(cfg.max_ix_query, con=con)
        start_ix = int(df_max_index.max_index.values[0]) + 1

    end_ix = start_ix + cfg.row_num_2_pull - 1
    return start_ix, end_ix


def print_end_time(identifier, start_time):
    end_time = datetime.now()
    print('{0}: end time: {1}'.format(identifier, end_time))
    print('{0}: duration: {1}'.format(identifier, end_time - start_time))


def get_data(start_ix, end_ix, start_time, identifier):
    try:
        query_4_ms = cfg.get_ms_data_query + "{0} and {1}".format(start_ix, end_ix)
        print('{0}: query for ms: {1}'.format(identifier,query_4_ms))
        df = utils.get_data_from_ms(query_4_ms)
        print('{0}: finish reading from ms'.format(identifier))
        if df is not None and not df.empty:
            for i in range(1, 3):
                if len(df) < cfg.min_rows_num and (datetime.now()-start_time).total_seconds() <= cfg.max_s_4_retry:
                    print('{0}: retry num: {1}'.format(identifier, i))
                    start_ix = df.mcext_rrid.max() + 1
                    end_ix = start_ix + (cfg.row_num_2_pull - len(df)) - 1
                    if not math.isnan(start_ix) and not math.isnan(end_ix):
                        query_4_ms = cfg.get_ms_data_query + "{0} and {1}".format(start_ix, end_ix)
                        x = utils.get_data_from_ms(query_4_ms)
                        df = pd.concat([df, x])
                else:
                    break
        return df
    except Exception as e:
        print("{0}: execution failed: {1}".format(identifier, e))
        return None


def write_2_s3(df_by_date, env):
    min_ix, max_ix = df_by_date.mcext_rrid.min(), df_by_date.mcext_rrid.max()
    cur_date = df_by_date.date.values[0]
    df_by_date = df_by_date.drop('date', axis=1)
    file_name = "{0}_{1}.csv".format(str(min_ix), str(max_ix))

    file_key = os.path.join(cfg.init_file_key, str(cur_date.year), str(cur_date.month).zfill(2),
                            str(cur_date.day).zfill(2), file_name)

    utils.write_df_2_s3(df_by_date, cfg.bucket, file_key, **cfg.s3_con.get(env))

    return max_ix


def get_df_4_s3(df, env):
    global_max_ix = 0
    for date, df_by_date in df.groupby('date'):
        cur_max = write_2_s3(df_by_date, env)
        if cur_max > global_max_ix:
            global_max_ix = cur_max

    return global_max_ix


def insert_2_sf(global_max_ix, num_of_rows, duration, min_dt, max_dt):

    curent_ts_utc = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    values = "'{0}','{1}',{2},'{3}','{4}','{5}'".format(
        curent_ts_utc, global_max_ix, num_of_rows,duration, min_dt, max_dt)
    utils.insert_to_db(cfg.insert_tbl_sf, values, cfg.insert_tbl_sf_cols)


def mcext_rr(env, identifier):
    start_time = datetime.now()
    print('{0}: start time: {1}'.format(identifier, datetime.now()))

    if get_running_status() == "locked":
        print("{0}: table locked".format(identifier))
        print_end_time(identifier, start_time)
        exit()
    else:
        update_running_status("locked")
        try:
            start_ix, end_ix = get_ix_range()
            df = get_data(start_ix, end_ix, start_time, identifier)

            if df is None or len(df) == 0:
                update_running_status("ready")
                print('{0}: no data to write, df is empty'.format(identifier))
                print_end_time(identifier, start_time)
                exit()
            else:
                num_of_rows = len(df)
                print('{0}: num of rows in df: {1}'.format(identifier, num_of_rows))

                min_dt, max_dt = df.datecreated.min(), df.datecreated.max()
                print("{0}: min date: {1}, max_date: {2}".format(identifier, min_dt, max_dt))

                df['date'] = df.datecreated.apply(lambda x: x.date())

                global_max_ix = get_df_4_s3(df, env)
                print('{0}: max id number: {1}'.format(identifier, global_max_ix))

                end_time = datetime.now()
                insert_2_sf(global_max_ix, num_of_rows, end_time - start_time, min_dt, max_dt)
                update_running_status("ready")
                print_end_time(identifier, start_time)
        except Exception as e:
            print("{0}: execution failed: {1}".format(identifier, e))
            update_running_status("ready")

