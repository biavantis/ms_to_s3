import pymssql
from src import configs
import pandas as pd
from io import StringIO
import boto3
import random
import string


def generate_identifier(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def get_data_from_ms(query):

    ms_conn = pymssql.connect(**configs.con_settings.get("ms"))
    df = pd.read_sql(query, con=ms_conn)
    df.columns = df.columns.str.lower()
    return df


def write_df_2_s3(df ,bucket, file_key, aws_access_key_id=None, aws_secret_access_key=None):

    csv_buffer = StringIO()
    df.to_csv(csv_buffer)
    if aws_access_key_id is not None:
        s3_resource = boto3.resource('s3', aws_access_key_id=aws_access_key_id,
                                     aws_secret_access_key=aws_secret_access_key)
    else:
        s3_resource = boto3.resource('s3')
    s3_resource.Object(bucket, file_key).put(Body=csv_buffer.getvalue())


def insert_to_db(tbl, values, tbl_cols, data_lst=None, method='single'):
    """


    :param tbl:  table to write into
    :param values: the values inside the 'insert query'
    :param tbl_cols: table's columns
    :param data_lst: optional, if more insert include more than one row. list of lists - list for every row we insert
    :param method: {single, many} insert method for one/more than one row.
    :return:
    """
    with pymssql.connect(**configs.con_settings.get("ms")) as con:
        with con.cursor() as cur:
            query = "insert into {0} ({1}) values ({2})".format(tbl, tbl_cols, values)
            if method == 'single':
                cur.execute(query)
            else:
                cur.executemany(query, data_lst)
            con.commit()


def execute_query(query):
    with pymssql.connect(**configs.con_settings.get("ms")) as ms_conn:
        with ms_conn.cursor() as cur:
            cur.execute(query)
            ms_conn.commit()

