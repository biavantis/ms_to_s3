import hudson.model.Result
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException

pipeline {
  agent any
  stages {
    stage('Pre') {  /* - - please do not touch the following step - - */
      steps {
        script {
          if (env.BUILD_NUMBER == '1') {
            echo "skips the first build to let jenkins load the parameters first"
            throw new FlowInterruptedException(Result.ABORTED)
          }
        }
        // handle build from commit ID
        sh """
        if [ "${params.COMMIT_ID}" != "" ]
        then
          git checkout -B ${params.COMMIT_ID}_jenkins
          git checkout ${params.COMMIT_ID} .
          git add .
          git commit -am "jenkins build from commit id ${params.COMMIT_ID}"
        fi
        """
        // versioning
        sh """#!/bin/bash -xe
        CURRENT_VERSION=\$(head -n 1 version.txt)
        semver=( \${CURRENT_VERSION//./\\ } )
        case ${params.VERSION_TYPE} in
            patch)
            NEW_VERSION="\$((semver[0])).\$((semver[1])).\$((semver[2]+1))"
            ;;
            minor)
            NEW_VERSION="\$((semver[0])).\$((semver[1]+1)).0"
            ;;
            major)
            NEW_VERSION="\$((semver[0]+1)).0.0"
            ;;
            *)
            NEW_VERSION=${params.VERSION_TYPE}
        esac
        echo \$NEW_VERSION > version.txt
        git add version.txt
        git commit -am \$NEW_VERSION
        """
        script {
          env.SNAPSHOTS_BUCKET = "build-snapshots"
          env.APP_VERSION = sh (script: "cat version.txt", returnStdout: true).trim()
          if (params.COMMIT_ID != '') {
            env.BRANCH_NAME = "${params.COMMIT_ID}_jenkins"
          }
          env.VERSION_ID = "${env.APP_VERSION}-${env.BRANCH_NAME}"
        }
      }
    }
    stage('App Params') {
      steps {
        script {
          env.APP_NAME = "ms_to_s3"
          env.BUILD_DIR = "${pwd()}/build"
        }
      }
    }
    stage('Build') {
      steps {
        sh """
          mkdir ${env.BUILD_DIR}
          #grep -v -x -f requirements-ds_layer.txt requirements.txt > prod-requirements.txt
          pip3.7 install -r requirements.txt -t ${env.BUILD_DIR}
          cp -r -p src ${env.BUILD_DIR}
          cp ./version.txt ${env.BUILD_DIR}
        """
      }
    }
    stage('Test') {
      steps {
        echo 'no tests!'
      }
    }
    stage('Deploy') {
      steps {
        script {  // mandatory step for backup
          env.BUILD_ZIPFILE = "${pwd()}/build.zip"
          echo 'Zip build files'
          // zips everything (including hidden files)
          sh """
            cd ${env.BUILD_DIR}
            zip -r9 ${env.BUILD_ZIPFILE} * .[^.]*
          """
        }
        echo 'Upload build to lambda'
        sh """
          LAMBDA_ARN="arn:aws:lambda:${params.REGION}:156785876661:function:${params.LAMBDA_NAME}"
          aws lambda update-function-code --function-name \$LAMBDA_ARN --zip-file fileb://${env.BUILD_ZIPFILE} --publish --region ${params.REGION}
        """
      }
    }
  }
  post {  /* - - please do not touch the following step - - */
    success {
      echo 'Upload build copy to s3'
      sh "aws s3 cp ${env.BUILD_ZIPFILE} s3://${env.SNAPSHOTS_BUCKET}/${env.APP_NAME}/${env.VERSION_ID}.zip"

      echo 'Update app version on repository'
      sshagent(credentials: ['bitbucket-jenkins']) {
        script {
          if (params.COMMIT_ID == '') {
            sh (script: "git push origin HEAD:${env.BRANCH_NAME}")
          } else {
            sh (script: "git push --set-upstream origin ${env.BRANCH_NAME}:${env.VERSION_ID}")
          }
        }
      }
      // sends slack realese success message
      script {
        def title = "The build of *${env.APP_NAME}@${env.VERSION_ID}* successfully deployed to production!"
        def description = "${params.VERSION_CHANGELOG}" ? "*Description:*\n${params.VERSION_CHANGELOG}\n" : ""
        def buildLink = "_<${env.RUN_CHANGES_DISPLAY_URL}|For more information see the build results page>_"
        wrap([$class: 'BuildUser']) {
          slackSend(channel: "${params.SLACK_CHANNEL}", color: 'good',
                    message: "${title}\n*User:* ${BUILD_USER}\n${description}${buildLink}")
        }
      }
    }
    failure {
      // sends slack realese failure message
      script {
        def title = "The build of *${env.APP_NAME}@${env.VERSION_ID}* has failed!"
        def buildLink = "_<${env.RUN_DISPLAY_URL}|For more information see the build results page>_"
        wrap([$class: 'BuildUser']) {
          slackSend(channel: "${params.SLACK_CHANNEL}", color: 'danger',
                    message: "${title}\n*User:* ${BUILD_USER}\n${buildLink}")
        }
      }
    }
    cleanup {
      cleanWs()  // clean up workspace data
    }
  }
  parameters {
    choice(name: 'VERSION_TYPE', choices: ['patch', 'minor', 'major'], description: 'The version type')
    string(name: 'COMMIT_ID', defaultValue: '', description: 'Build from commit (HEAD if empty)')
    string(name: 'SLACK_CHANNEL', defaultValue: '#bi-changelog', description: 'The tracking slack channel')
    text(name: 'VERSION_CHANGELOG', defaultValue: '', description: 'The version changelog description')
    string(name: 'LAMBDA_NAME', defaultValue: 'ms_to_s3', description: 'The lambda function name')
    string(name: 'REGION', defaultValue: 'us-west-2', description: 'The lambda region')
  }
}